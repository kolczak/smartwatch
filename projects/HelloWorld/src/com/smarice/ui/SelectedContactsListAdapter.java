package com.smarice.ui;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.smarice.R;
import com.smarice.model.Contact;

public class SelectedContactsListAdapter extends BaseAdapter implements View.OnClickListener {

	private Context mContext;
    private Listener mListener;
	private List<Contact> mContactsList;

	public SelectedContactsListAdapter(Context context, Set<Contact> contactsSet) {
		super();
		mContext = context;
		mContactsList = new ArrayList<Contact>();
		mContactsList.addAll(Arrays.asList(contactsSet.toArray(new Contact[0])));
	}

	public void setContacts(Set<Contact> contactsSet)
	{
		mContactsList.clear();
		mContactsList.addAll(Arrays.asList(contactsSet.toArray(new Contact[0])));
		notifyDataSetChanged();
	}

    public void setListener(Listener listener) {
        mListener = listener;
    }
	
	@Override
	public int getCount() {
		return mContactsList.size();
	}

	@Override
	public Contact getItem(int position) {
		return mContactsList.get(position);
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View rowView, ViewGroup arg2) {
		
		if (rowView == null)
		{
			rowView = LayoutInflater.from(mContext).inflate(R.layout.selected_contact_row, null);
		}

        Contact contact = getItem(position);
		TextView contactName = (TextView)rowView.findViewById(R.id.selected_contact_row_contact_name);
        Button removeContactBtn = (Button) rowView.findViewById(R.id.remove_contact_btn);
        removeContactBtn.setTag(position);
        removeContactBtn.setOnClickListener(this);
		contactName.setText(contact.getName());
		
		return rowView;
	}

    @Override
    public void onClick(View v) {
        int index = (Integer) v.getTag();
        Contact contact = getItem(index);
        mContactsList.remove(index);
        notifyDataSetChanged();
        if(mListener != null) mListener.onContactRemoved(contact);
    }

    public interface Listener {
        public void onContactRemoved(Contact contact);
    }
}
