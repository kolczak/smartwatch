package com.smarice.model;

public class AccelerometerCoordinates {
	private float mX;
	private float mY;
	private float mZ;
	private long mTimestamp;

	public AccelerometerCoordinates(float x, float y, float z, long timestamp) {
		mX = x;
		mY = y;
		mZ = y;
		mTimestamp = timestamp;
	}
	
	public long getTimestamp() {
		return mTimestamp;
	}

	public void setTimestamp(long timestamp) {
		this.mTimestamp = timestamp;
	}

	public float getZ() {
		return mZ;
	}

	public void setZ(float z) {
		this.mZ = z;
	}

	public float getY() {
		return mY;
	}

	public void setY(float y) {
		this.mY = y;
	}

	public float getX() {
		return mX;
	}

	public void setX(float x) {
		this.mX = x;
	}
}
