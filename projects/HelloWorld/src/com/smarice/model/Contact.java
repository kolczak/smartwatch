package com.smarice.model;

import java.io.Serializable;

public class Contact implements Serializable {
    private static final long serialVersionUID = 0x578;
    protected String name;
    protected String phone;

    public Contact(String name, String phone) {
        if(phone == null) throw new IllegalArgumentException("A contact must have a phone");
        this.name = (name == null) ? "Unnamed" : name;
        this.phone = phone;
    }

    @Override
    public String toString() {
        return name + " - " + phone;
    }

    @Override
    public int hashCode() {
        return phone.hashCode();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }
}
