package com.smarice.utils;

import android.location.Address;

public class LocationUtils {

    public static String addressToString(Address address) {
        String country = address.getCountryName();
        String locality = address.getLocality();
        String throughFare = address.getThoroughfare();
        String subthroughFare = address.getSubThoroughfare();
        String street = throughFare;
        if(subthroughFare != null) street += " " + subthroughFare;
        String[] comps = {street, locality, country};
        return buildAddress(comps);
    }

    private static String buildAddress(String[] comps) {
        String address = "";
        for(String comp : comps) {
            if(comp != null) {
                address += comp + ", ";
            }
        }
        return address.substring(0, address.length() - 2);
    }
}
