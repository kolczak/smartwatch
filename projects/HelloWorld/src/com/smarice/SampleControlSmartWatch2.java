/*
Copyright (c) 2011, Sony Ericsson Mobile Communications AB
Copyright (c) 2011-2013, Sony Mobile Communications AB

 All rights reserved.

 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:

 * Redistributions of source code must retain the above copyright notice, this
 list of conditions and the following disclaimer.

 * Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution.

 * Neither the name of the Sony Ericsson Mobile Communications AB / Sony Mobile
 Communications AB nor the names of its contributors may be used to endorse or promote
 products derived from this software without specific prior written permission.

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package com.smarice;

import java.io.IOException;
import java.util.List;

import android.annotation.SuppressLint;
import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Looper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Toast;

import com.smarice.model.AccelerometerCoordinates;
import com.smarice.persistance.ContactController;
import com.smarice.utils.LocationUtils;
import com.sonyericsson.extras.liveware.aef.control.Control;
import com.sonyericsson.extras.liveware.aef.registration.Registration.SensorTypeValue;
import com.sonyericsson.extras.liveware.aef.sensor.Sensor;
import com.sonyericsson.extras.liveware.extension.util.control.ControlExtension;
import com.sonyericsson.extras.liveware.extension.util.control.ControlObjectClickEvent;
import com.sonyericsson.extras.liveware.extension.util.control.ControlTouchEvent;
import com.sonyericsson.extras.liveware.extension.util.control.ControlView;
import com.sonyericsson.extras.liveware.extension.util.control.ControlView.OnClickListener;
import com.sonyericsson.extras.liveware.extension.util.control.ControlViewGroup;
import com.sonyericsson.extras.liveware.extension.util.registration.DeviceInfoHelper;
import com.sonyericsson.extras.liveware.extension.util.sensor.AccessorySensor;
import com.sonyericsson.extras.liveware.extension.util.sensor.AccessorySensorEvent;
import com.sonyericsson.extras.liveware.extension.util.sensor.AccessorySensorEventListener;
import com.sonyericsson.extras.liveware.extension.util.sensor.AccessorySensorException;
import com.sonyericsson.extras.liveware.extension.util.sensor.AccessorySensorManager;

/**
 * The sample control for SmartWatch handles the control on the accessory. This
 * class exists in one instance for every supported host application that we
 * have registered to
 */
class SampleControlSmartWatch2 extends ControlExtension {

	private static final int VIBRATION_REPEAT = 2;
	private static final int VIBRATION_ON_DURATION = 3000; //30 sec
	private static final int VIBRATION_OFF_DURATION = 1000; //30 sec
	private static final long ACCEPTABLE_OFFSET_FROM_LAST_MOVE = 10; //30 sec
	private static final float ACCEPTABLE_MOVEMENT_TRESHOLD = 0.5f; 
	
	private boolean buttonClicked = false;
	private ControlViewGroup mLayout = null;
	private AccessorySensor mSensor;
	private AccelerometerCoordinates lastCoordinates;
    private final AccessorySensorEventListener mListener = new AccessorySensorEventListener() {

        @Override
        public void onSensorEvent(AccessorySensorEvent sensorEvent) {
            Log.d(SampleExtensionService.LOG_TAG, "Listener: OnSensorEvent");
            updateCurrentDisplay(sensorEvent);
        }

    };
	
	/**
	 * Create sample control.
	 * 
	 * @param hostAppPackageName
	 *            Package name of host application.
	 * @param context
	 *            The context.
	 * @param handler
	 *            The handler to use
	 */
	SampleControlSmartWatch2(final String hostAppPackageName,
			final Context context) {
		super(context, hostAppPackageName);
		
		setupClickables(context);
		
		AccessorySensorManager manager = new AccessorySensorManager(context, hostAppPackageName);
		if (DeviceInfoHelper.isSensorSupported(context, hostAppPackageName, SensorTypeValue.ACCELEROMETER))
			mSensor = manager.getSensor(SensorTypeValue.ACCELEROMETER);
	}

	private void updateCurrentDisplay(AccessorySensorEvent sensorEvent) {
		updateGenericSensorDisplay(sensorEvent, mSensor.getType().getName());
    }
	
	private void updateGenericSensorDisplay(AccessorySensorEvent sensorEvent,
			String name) {

		if (sensorEvent != null) {
            float[] values = sensorEvent.getSensorValues();

            if (values != null && values.length == 3) {
                // Show values with one decimal.
            	detectMovement(values[0], values[1], values[2], (long) (sensorEvent.getTimestamp() / 1e9));
            }

            // Show time stamp in milliseconds. (Reading is in nanoseconds.)
            //(long) (sensorEvent.getTimestamp() / 1e9);

            // Show sensor accuracy.
            sensorEvent.getAccuracy();
        }
	}

	private void detectMovement(float x, float y, float z, long timestamp) {
		AccelerometerCoordinates newCoordinates = new AccelerometerCoordinates(x, y, z, timestamp);
		
		if (lastCoordinates == null) {
			lastCoordinates = newCoordinates;
			return;
		}
		
		if (personMoved(newCoordinates)) {
			lastCoordinates = newCoordinates;
		} else if (lastCoordinates.getTimestamp() + ACCEPTABLE_OFFSET_FROM_LAST_MOVE < timestamp) {
			Toast.makeText(mContext, "No move detected, texting that its serious", Toast.LENGTH_SHORT).show();
			Log.d("KONRAD", "No move detected, texting that its serious");
			
			startVibrator(VIBRATION_ON_DURATION, VIBRATION_OFF_DURATION, VIBRATION_REPEAT);
			
			//TODO: display advise to move hand and run some vibration
			//TODO: inform somebody
			lastCoordinates = newCoordinates;	//this resets time stamp
		}
		
		Log.d("KONRAD", "x="+x+", y="+y+", z="+z);
		if (timestamp % 10 == 0)
			Toast.makeText(mContext, "x="+x+", y="+y+", z="+z, Toast.LENGTH_SHORT).show();



		
	}

	private boolean personMoved(AccelerometerCoordinates newCoordinates) {
		if (Math.abs(lastCoordinates.getX() - newCoordinates.getX()) > ACCEPTABLE_MOVEMENT_TRESHOLD ||
				Math.abs(lastCoordinates.getY() - newCoordinates.getY()) > ACCEPTABLE_MOVEMENT_TRESHOLD ||
				Math.abs(lastCoordinates.getZ() - newCoordinates.getZ()) > ACCEPTABLE_MOVEMENT_TRESHOLD)
			return true;
		return false;
	}

	private void register() {
        Log.d(SampleExtensionService.LOG_TAG, "Register listener");
        if (mSensor != null) {
            try {
                if (mSensor.isInterruptModeSupported()) {
                	mSensor.registerInterruptListener(mListener);
                } else {
                	mSensor.registerFixedRateListener(mListener,
                            Sensor.SensorRates.SENSOR_DELAY_UI);
                }
            } catch (AccessorySensorException e) {
                Log.d(SampleExtensionService.LOG_TAG, "Failed to register listener", e);
            }
        }
    }

    private void unregister() {
        if (mSensor != null) {
        	mSensor.unregisterListener();
        }
    }

    private void unregisterAndDestroy() {
        unregister();
        mSensor = null;
    }
	
    
	/**
	 * Get supported control width.
	 * 
	 * @param context
	 *            The context.
	 * @return the width.
	 */
	public static int getSupportedControlWidth(Context context) {
		return context.getResources().getDimensionPixelSize(
				R.dimen.smart_watch_2_control_width);
	}

	/**
	 * Get supported control height.
	 * 
	 * @param context
	 *            The context.
	 * @return the height.
	 */
	public static int getSupportedControlHeight(Context context) {
		return context.getResources().getDimensionPixelSize(
				R.dimen.smart_watch_2_control_height);
	}

	@Override
	public void onDestroy() {
		Log.d(SampleExtensionService.LOG_TAG,
                "SampleControlSmartWatch onDestroy");
		unregisterAndDestroy();
	};

	@Override
	public void onStart() {
		// Nothing to do. Animation is handled in onResume.
	}

	@Override
	public void onStop() {
		// Nothing to do. Animation is handled in onPause.
	}

	@Override
	public void onResume() {
		Log.d(SampleExtensionService.LOG_TAG, "Starting animation");

		Bundle[] data = new Bundle[0];

		showLayout(R.layout.sw2_main_layout, data);

		register();
		
		setScreenState(Control.Intents.SCREEN_STATE_ON);
	}

	@Override
	public void onTouch(final ControlTouchEvent event) {
		Log.d(SampleExtensionService.LOG_TAG, "onTouch() " + event.getAction());
		if (event.getAction() == Control.Intents.TOUCH_ACTION_RELEASE) {
			Log.d(SampleExtensionService.LOG_TAG, "Toggling animation");
		}
	}

	@Override
	public void onObjectClick(final ControlObjectClickEvent event) {
		Log.d(SampleExtensionService.LOG_TAG,
				"onObjectClick() " + event.getClickType());
		if (event.getLayoutReference() != -1) {
			mLayout.onClick(event.getLayoutReference());
		}
	}

	@Override
	public void onKey(final int action, final int keyCode, final long timeStamp) {
		Log.d(SampleExtensionService.LOG_TAG, "onKey()");
		if (action == Control.Intents.KEY_ACTION_RELEASE
				&& keyCode == Control.KeyCodes.KEYCODE_OPTIONS) {
			Log.d(SampleExtensionService.LOG_TAG,
                    "onKey() - options button intercepted.");
		} else if (action == Control.Intents.KEY_ACTION_RELEASE
				&& keyCode == Control.KeyCodes.KEYCODE_BACK) {
			Log.d(SampleExtensionService.LOG_TAG,
					"onKey() - back button intercepted.");
		}
	}

	private void setupClickables(Context context) {
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View layout = inflater.inflate(R.layout.sw2_main_layout, null);
		mLayout = (ControlViewGroup) parseLayout(layout);
		if (mLayout != null) {
			Log.d("KONRAD", "setupClickables mLayout not null");
			ControlView sayHelloButton = mLayout.findViewById(R.id.sw2_main_layout_emergency_button);
			sayHelloButton.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick() {
					if (!buttonClicked)
						sendTextMessages();
					buttonClicked = true;
				}
			});
		}
	}

    private Location getLastKnownLocation() {
        LocationManager manager = (LocationManager) mContext
                .getSystemService(Context.LOCATION_SERVICE);
        Location networkLocation = manager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
        Location gpsLocation = manager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        return (gpsLocation != null) ? gpsLocation : networkLocation;
    }

    @SuppressLint("NewApi")
    private void fetchLocationAndSendAsync() {
        Looper looper = Looper.getMainLooper();
        LocationManager manager = (LocationManager) mContext
                .getSystemService(Context.LOCATION_SERVICE);
        manager.requestSingleUpdate(LocationManager.NETWORK_PROVIDER,
                new LocationListener() {
                    @Override
                    public void onLocationChanged(Location location) {
                        sendTextMessagesAsync(location, null);
                    }

                    @Override
                    public void onStatusChanged(String provider, int status, Bundle extras) {
                    }

                    @Override
                    public void onProviderEnabled(String provider) {
                    }

                    @Override
                    public void onProviderDisabled(String provider) {
                    }
                }, looper);
    }

	@SuppressLint("NewApi")
    protected void sendTextMessages() {
		Log.d("KONRAD", "sending messages");
        Location location = getLastKnownLocation();
        if(location != null) {
            //sendTextMessagesAsync(location, null);
        } else {
            //fetchLocationAndSendAsync();
        }
        Toast.makeText(mContext, "message sent", Toast.LENGTH_SHORT).show();
	}

	protected void sendTextMessagesAsync(final Location location, String text) {
        final ContactController contactController = ContactController.getInstance(mContext);
        if(text != null) {
            contactController.textContacts(text);
            return;
        }
        new Thread(new Runnable() {
            @Override
            public void run() {
                Geocoder geocoder = new Geocoder(mContext);
                double lat = location.getLatitude();
                double lon = location.getLongitude();
                List<Address> addresses = null;
                try {
                    addresses = geocoder.getFromLocation(lat, lon, 1);
                } catch (IOException e) {
                    e.printStackTrace();
                }

                String message;
                if(addresses != null && addresses.size() > 0) {
                    String address = LocationUtils.addressToString(addresses.get(0));
                    message = "I need help! I'm located near " + address;
                } else {
                    message = "I need help! I'm located near " + lat + ", " + lon;
                }
                contactController.textContacts(message);
            }
        }).start();
    }
}
