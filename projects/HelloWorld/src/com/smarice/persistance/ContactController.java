package com.smarice.persistance;

import android.content.Context;
import android.telephony.SmsManager;
import com.smarice.model.Contact;
import org.apache.commons.io.IOUtils;

import java.io.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class ContactController {
    private static final String FILE_NAME = "saved_contacts";
    public static ContactController sharedInstance;

    private Set<Contact> mContacts;

    /**
     * Static factory constructor
     * @return ContactController instance
     */
    public static ContactController getInstance(Context context) {
        if(sharedInstance == null) {
            sharedInstance = new ContactController(context);
        }
        return sharedInstance;
    }

    private ContactController(Context context) {
        mContacts = loadContacts(context);
    }

    /**
     * Adds given phone number to all
     * @param phoneNumber The number to add
     */
    public void addContact(String phoneNumber) {
        if(phoneNumber == null) {
            throw new IllegalArgumentException("Phone number cannot be null");
        }
        mContacts.add(new Contact(phoneNumber, null));
    }

    public void addContact(String phoneNumber, String name) {
        mContacts.add(new Contact(name, phoneNumber));
    }

    public boolean deleteContact(Contact contact) {
        return mContacts.remove(contact);
    }

    /**
     * Sends an SMS message to all stored phone numbers
     * @param msg The message to send
     */
    public void textContacts(String msg) {
        SmsManager manager = SmsManager.getDefault();
        ArrayList<String> parts = manager.divideMessage(msg);
        for(Contact contact : mContacts) {
            String number = contact.getPhone();
            manager.sendMultipartTextMessage(number, null, parts, null, null);
        }
    }

    public boolean saveContacts(Context context) {
        if(mContacts == null) return false;
        boolean result = false;
        ObjectOutputStream outputStream = null;
        try {
            outputStream = new ObjectOutputStream(context.openFileOutput(FILE_NAME, Context.MODE_PRIVATE));
            outputStream.writeObject(mContacts);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            IOUtils.closeQuietly(outputStream);
        }
        return result;
    }

    @SuppressWarnings("unchecked")
    public Set<Contact> loadContacts(Context context) {
        Set<Contact> results = null;
        ObjectInputStream inputStream = null;
        try {
            inputStream = new ObjectInputStream(context.openFileInput(FILE_NAME));
            results = (Set<Contact>) inputStream.readObject();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (StreamCorruptedException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            IOUtils.closeQuietly(inputStream);
        }
        return (results != null) ? results : new HashSet<Contact>();
    }
    
    public Set<Contact> getContacts() {
    	return mContacts;
    }
    
    public void clearContacts() {
    	mContacts.clear();
    }
}
