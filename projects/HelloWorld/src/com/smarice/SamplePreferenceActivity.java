/*
Copyright (c) 2011, Sony Ericsson Mobile Communications AB
Copyright (c) 2011-2013, Sony Mobile Communications AB

 All rights reserved.

 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:

 * Redistributions of source code must retain the above copyright notice, this
 list of conditions and the following disclaimer.

 * Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution.

 * Neither the name of the Sony Ericsson Mobile Communications AB / Sony Mobile
 Communications AB nor the names of its contributors may be used to endorse or promote
 products derived from this software without specific prior written permission.

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package com.smarice;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.provider.ContactsContract;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import com.smarice.model.Contact;
import com.smarice.persistance.ContactController;
import com.smarice.ui.SelectedContactsListAdapter;

/**
 * The sample control preference activity handles the preferences for the sample
 * control extension.
 */
public class SamplePreferenceActivity extends PreferenceActivity {
    private static final int REQUEST_PICK_CONTACTS = 0x456;
    private static final int DIALOG_READ_ME = 1;

    private SelectedContactsListAdapter mAdapter = null;
    private ListView mList;

    @SuppressWarnings("deprecation")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        Button pickContact = (Button) findViewById(R.id.activity_main_pick_contact_button);
        mList = (ListView) findViewById(android.R.id.list);
        pickContact.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                pickContacts();
            }
        });
        displaySelectedContacts();

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Uri contactUri = data.getData();
        if (requestCode == REQUEST_PICK_CONTACTS && resultCode == Activity.RESULT_OK && contactUri != null) {
            fetchPhonesFromContact(contactUri.getLastPathSegment());
            displaySelectedContacts();
        }
    }

    private void displaySelectedContacts() {
        final ContactController contactController = ContactController.getInstance(this);

        if (contactController.getContacts().size() == 0)
            return;

        if (mAdapter == null) {
            mAdapter = new SelectedContactsListAdapter(this, contactController.getContacts());
            mAdapter.setListener(new SelectedContactsListAdapter.Listener() {
                @Override
                public void onContactRemoved(Contact contact) {
                    contactController.deleteContact(contact);
                    contactController.saveContacts(getApplicationContext());
                }
            });
            mList.setAdapter(mAdapter);
        } else {
            mAdapter.setContacts(contactController.getContacts());
        }
    }

    private void pickContacts() {
        Intent intent = new Intent(
                Intent.ACTION_PICK,
                ContactsContract.Contacts.CONTENT_URI
        );
        startActivityForResult(intent, REQUEST_PICK_CONTACTS);
    }

    private void fetchPhonesFromContact(String contactId) {
        //TODO Do this in a loader?
        ContentResolver resolver = getContentResolver();
        Uri phoneUri = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
        String where = ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = " + contactId;
        Cursor cursor = resolver.query(phoneUri, null, where, null, null);
        if (cursor != null && cursor.moveToFirst()) {
            Context context = getApplicationContext();
            ContactController contactController = ContactController.getInstance(context);
            try {
                while (!cursor.isAfterLast()) {
                    int phoneCol = cursor.getColumnIndexOrThrow(
                            ContactsContract.CommonDataKinds.Phone.NUMBER);
                    int nameCol = cursor.getColumnIndexOrThrow(
                            ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME
                    );
                    String number = cursor.getString(phoneCol);
                    String name = cursor.getString(nameCol);
                    contactController.addContact(number, name);
                    cursor.moveToNext();
                }
            } finally {
                cursor.close();
            }
            contactController.saveContacts(context);
        }
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        Dialog dialog = null;

        switch (id) {
            case DIALOG_READ_ME:
                dialog = createReadMeDialog();
                break;
            default:
                Log.w(SampleExtensionService.LOG_TAG, "Not a valid dialog id: " + id);
                break;
        }

        return dialog;
    }

    /**
     * Create the Read me dialog
     *
     * @return the Dialog
     */
    private Dialog createReadMeDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.preference_option_read_me_txt)
                .setTitle(R.string.preference_option_read_me)
                .setIcon(android.R.drawable.ic_dialog_info)
                .setPositiveButton(android.R.string.ok, new OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
        return builder.create();
    }

}
